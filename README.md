## Sway config

Sway config is split to 2 parts:

 * `sway/config` &mdash; is the main config to be installed to `/etc/sway/config`. Some light changes are applied, such as removing things we want to override later (bar, launcher, etc.).
 * `sway/config.d/*.conf` &mdash; is a set of small self-contained configuration snippets with each containing the description and the list of required packages.
   Will be installed as `/usr/share/sway/config.d/*.conf` and loaded by default.

## Upstream config changes sync

The process described below should used to update the configuration for a new upstream project release. The goal is to limit merges to changes since last release and to simplify tracking our local modifications.

1. Switch to the `upstream` branch
1. Overwrite the configuration file with an updated one
1. Do any pre-processing if necessary, e.g. replace variables, paths or anything else upstream buildsystem does before installing the file.
1. Commit changes, specifying the upstream release or tag in the commit message. Example: `sway: sync config with upstream release 1.8`
1. Switch to the `fedora` branch and merge changes from `upstream`
1. Update required project version in the RPM spec as necessary.

